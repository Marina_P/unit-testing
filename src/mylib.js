const mylib = {
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    divide: (a, b) => {
        if (b == 0) {
            console.error("dividing by 0 is inpossible");
            return false;
        } else {
            return a / b;
        }

    },
    multiply: (a, b) => {
        return a * b;
    }
};

module.exports = mylib;