const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // initialization
        // create object.. etc..
        console.log("Initialising completed.");
    });
    it("Can add 1 and 2 together", () => {
        expect(mylib.add(1, 2)).equal(3, "1 + 2 is not 3, for some reason?");
    });
    it("Can subtract 3 and 3", () => {
        expect(mylib.subtract(3, 3)).equal(0, "3 - 3 is not 0, for some reason?");
    });
    it("Can devide 8 by 0", () => {
        expect(mylib.divide(8, 0)).equal(false, "Divide by 0 is inpossible");
    });
    it("Can multiply 2 to 2", () => {
        expect(mylib.multiply(2, 2)).equal(4, "2 * 2 is not 4, fo some reason?");
    });
    after(() => {
        //Cleanup
        // For example: shutdown the Express server. 
        console.log("Testing completed!");
    })
});