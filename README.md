# Unit Testing task

1. Create unit tests for your JavaScript module mylib
2. Write one test suite where one function executes before the testing starts and one function that runs after the testing is completed
3. <mark>Handle the "ZeroDivision" in the mylib module by trowing an error for 0 divisor. Then create one unit test where it sends 0 as a divisor and expects it to throw an error. </mark>
4. Make sure there are atleast one test per function.
5. Confirm that the unit tests and the library are working.


## end! :100:

